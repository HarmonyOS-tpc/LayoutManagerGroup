/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openharmony.recyclercomponent;

import com.ohos.RecyclerContainer;
import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * HarmonyConstant
 */
public class HarmonyConstant {
    /**
     * LAYOUT_DIRECTION_LTR
     */
    public static final int LAYOUT_DIRECTION_LTR = 0;

    /**
     * LAYOUT_DIRECTION_RTL
     */
    public static final int LAYOUT_DIRECTION_RTL = 1;

    /**
     * LAYOUT_DIRECTION_INHERIT
     */
    public static final int LAYOUT_DIRECTION_INHERIT = 2;

    /**
     * LAYOUT_DIRECTION_LOCALE
     */
    public static final int LAYOUT_DIRECTION_LOCALE = 3;

    public static final int SDK_INT = 14;

    /**
     * GRAVITY_EARTH
     */
    public static final float GRAVITY_EARTH = 9.80665f;

    /**
     * getLayoutDirection
     *
     * @param view RecyclerContainer
     * @return int
     */
    public static int getLayoutDirection(RecyclerContainer view) {
        return LAYOUT_DIRECTION_LTR;
    }

    /**
     * getDp
     *
     * @param context Context
     * @return int
     */
    public static int getDp(Context context) {
        return DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes().densityDpi;
    }

    /**
     * getDensity
     *
     * @param context Context
     * @return int
     */
    public static float getDensity(Context context) {
        return DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes().densityDpi;
    }

    /**
     * getMaximumWindowMetrics
     *
     * @param context Context
     * @return Rect
     */
    public static Rect getMaximumWindowMetrics(Context context) {
        Point size = new Point();
        DisplayManager.getInstance().getDefaultDisplay(context).get().getSize(size);
        return new Rect(0, 0, size.getPointXToInt(), size.getPointYToInt());
    }

    /**
     * getThemeColor
     *
     * @return int
     */
    public static int getThemeColor() {
        return 0xff666666;
    }

    /**
     * currentAnimationTimeMillis
     *
     * @return long
     */
    public static long currentAnimationTimeMillis() {
        return 0;
    }
}
