package com.dingmouren.layoutmanagergroup.echelon;

/**
 * Created by 钉某人
 * github: https://github.com/DingMouRen
 * email: naildingmouren@gmail.com
 */
public class ItemViewInfo {
    private float mScaleXY;
    private float mLayoutPercent;
    private float mPositionOffset;
    private int mTop;
    private boolean mIsBottom;

    /**
     * ItemViewInfo
     *
     * @param top           int
     * @param scaleXY       float
     * @param positonOffset float
     * @param percent       float
     */
    public ItemViewInfo(int top, float scaleXY, float positonOffset, float percent) {
        this.mTop = top;
        this.mScaleXY = scaleXY;
        this.mPositionOffset = positonOffset;
        this.mLayoutPercent = percent;
    }

    /**
     * ItemViewInfo set
     *
     * @return mIsBottom
     */
    public ItemViewInfo setIsBottom() {
        mIsBottom = true;
        return this;
    }

    /**
     * getScaleXY set
     *
     * @return mScaleXY
     */
    public float getScaleXY() {
        return mScaleXY;
    }

    /**
     * setScaleXY
     *
     * @param mScaleXY float
     */
    public void setScaleXY(float mScaleXY) {
        this.mScaleXY = mScaleXY;
    }

    /**
     * setScaleXY
     *
     * @return mLayoutPercent float
     */
    public float getLayoutPercent() {
        return mLayoutPercent;
    }

    /**
     * setLayoutPercent
     *
     * @param mLayoutPercent float
     */
    public void setLayoutPercent(float mLayoutPercent) {
        this.mLayoutPercent = mLayoutPercent;
    }

    /**
     * getPositionOffset
     *
     * @return mPositionOffset float
     */
    public float getPositionOffset() {
        return mPositionOffset;
    }

    /**
     * getPositionOffset
     *
     * @param mPositionOffset float
     */
    public void setPositionOffset(float mPositionOffset) {
        this.mPositionOffset = mPositionOffset;
    }

    /**
     * getTop
     *
     * @return mTop
     */
    public int getTop() {
        return mTop;
    }

    /**
     * setTop
     *
     * @param mTop int
     */
    public void setTop(int mTop) {
        this.mTop = mTop;
    }
}
