/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dingmouren.layoutmanagergroup.sample.slice;

import com.dingmouren.layoutmanagergroup.sample.MainAbility;
import com.dingmouren.layoutmanagergroup.sample.ResourceTable;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;

/**
 * Function description
 * class MainSlice
 */
public class MainSlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_menu);
        findComponentById(ResourceTable.Id_one).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Operation systemOperation = new Intent.OperationBuilder().withAction("action.echalon")
                        .withBundleName(getBundleName())
                        .withAbilityName(MainAbility.class.getSimpleName())
                        .build();
                intent.setOperation(systemOperation);
                startAbility(intent);
            }
        });
        findComponentById(ResourceTable.Id_two).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Operation systemOperation = new Intent.OperationBuilder().withAction("action.skid")
                        .withBundleName(getBundleName())
                        .withAbilityName(MainAbility.class.getSimpleName())
                        .build();
                intent.setOperation(systemOperation);
                startAbility(intent);
            }
        });
    }
}
