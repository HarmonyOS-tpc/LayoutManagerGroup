/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dingmouren.layoutmanagergroup.sample.adapter;

import com.ohos.RecyclerContainer;
import com.dingmouren.layoutmanagergroup.sample.ResourceTable;
import com.dingmouren.layoutmanagergroup.utils.LogUtil;
import com.openharmony.recyclercomponent.ContainerGroup;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Function description
 * class AdapterOriginal
 */
public class SkidRightAdapter extends RecyclerContainer.Adapter {
    private String[] titles = {"Acknowl", "Belief", "Confidence", "Dreaming", "Happiness", "Confidence"};

    public SkidRightAdapter() {
    }

    @Override
    public RecyclerContainer.ViewHolder createViewHolder(ContainerGroup parent, int viewType) {
        return new SkidRightViewHolder(LayoutScatter.getInstance(parent.getContext())
                .parse(ResourceTable.Layout_item_skid_right, null, false));
    }

    @Override
    public void bindViewHolder(RecyclerContainer.ViewHolder holder, int position) {
        SkidRightViewHolder skidRightViewHolder = null;
        if (holder instanceof SkidRightViewHolder) {
            skidRightViewHolder = (SkidRightViewHolder) holder;
        }
        assert skidRightViewHolder != null;
        int background;
        switch (position % 7) {
            case 0:
                background = ResourceTable.Graphic_tree;
                break;
            case 1:
                background = ResourceTable.Graphic_tree1;
                break;
            case 2:
                background = ResourceTable.Graphic_tree2;
                break;
            case 3:
                background = ResourceTable.Graphic_tree3;
                break;
            case 4:
                background = ResourceTable.Graphic_tree4;
                break;
            case 5:
                background = ResourceTable.Graphic_tree5;
                break;
            case 6:
                background = ResourceTable.Graphic_tree6;
                break;
            default:
                background = ResourceTable.Media_header_icon_1;
        }
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.BLACK.getValue()));
        skidRightViewHolder.itemView.setBackground(shapeElement);
        try {
            skidRightViewHolder.ivBg.setPixelMap(preparePixelmap(skidRightViewHolder.ivBg.getResourceManager()
                    .getResource(background)));
            skidRightViewHolder.tvTitle.setText(titles[position % 6]);
        } catch (IOException | NotExistException e) {
            LogUtil.error("View", "" + e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return 20;
    }

    static class SkidRightViewHolder extends RecyclerContainer.ViewHolder {
        private Text tvTitle, tvBottom;
        private Image ivBg;

        SkidRightViewHolder(Component itemView) {
            super(itemView);
            if (itemView.findComponentById(ResourceTable.Id_tv_title) instanceof Text) {
                tvTitle = (Text) itemView.findComponentById(ResourceTable.Id_tv_title);
            }
            if (itemView.findComponentById(ResourceTable.Id_tv_bottom) instanceof Text) {
                tvBottom = (Text) itemView.findComponentById(ResourceTable.Id_tv_bottom);
            }
            if (itemView.findComponentById(ResourceTable.Id_iv_Banner) instanceof Image) {
                ivBg = (Image) itemView.findComponentById(ResourceTable.Id_iv_Banner);
            }
        }
    }

    private static PixelMap preparePixelmap(Resource resource) throws IOException {
        ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
        ImageSource imageSource;
        try {
            imageSource = ImageSource.create(readResource(resource), srcOpts);
        } finally {
            resource.close();
        }
        if (imageSource == null) {
            LogUtil.debug("File", "Not Found");
        }
        ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
        decodingOpts.desiredSize = new Size(0, 0);
        decodingOpts.desiredRegion = new Rect(0, 0, 0, 0);
        decodingOpts.desiredPixelFormat = PixelFormat.ARGB_8888;

        return imageSource.createPixelmap(decodingOpts);
    }

    private static byte[] readResource(Resource resource) throws IOException {
        final int bufferSize = 1024;
        final int ioEnd = -1;

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buffer = new byte[bufferSize];
        while (true) {
            try {
                int readLen = resource.read(buffer, 0, bufferSize);
                if (readLen == ioEnd) {
                    break;
                }
                output.write(buffer, 0, readLen);
            } catch (IOException e) {
                break;
            } finally {
                output.close();
            }
        }
        return output.toByteArray();
    }
}