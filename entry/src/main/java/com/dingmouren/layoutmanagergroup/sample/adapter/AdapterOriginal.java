/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dingmouren.layoutmanagergroup.sample.adapter;

import com.dingmouren.layoutmanagergroup.utils.LogUtil;
import com.dingmouren.layoutmanagergroup.sample.ResourceTable;
import com.openharmony.recyclercomponent.ContainerGroup;
import com.ohos.RecyclerContainer;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.global.resource.NotExistException;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.Size;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Function description
 * class AdapterOriginal
 */
public class AdapterOriginal extends RecyclerContainer.Adapter {
    private ArrayList<String> list = new ArrayList<>();
    private boolean verticalList;

    public AdapterOriginal(boolean verticalList) {
        list.add("FIRST");
        list.add("SECOND");
        list.add("THIRD");
        list.add("FOURTH");
        list.add("FIFTH");
        list.add("SIX");
        this.verticalList = verticalList;
    }

    @Override
    public RecyclerContainer.ViewHolder createViewHolder(ContainerGroup parent, int viewType) {
        if (verticalList) {
            return new ViewHolderOriginal(LayoutScatter.getInstance(parent.getContext())
                    .parse(ResourceTable.Layout_list, null, false));
        } else {
            return new ViewHolderOriginal(LayoutScatter.getInstance(parent.getContext())
                    .parse(ResourceTable.Layout_item, null, false));
        }
    }

    @Override
    public void bindViewHolder(RecyclerContainer.ViewHolder holder, int position) {
        ViewHolderOriginal viewHolderOriginal = null;
        if (holder instanceof ViewHolderOriginal) {
            viewHolderOriginal = (ViewHolderOriginal) holder;
        }
        assert viewHolderOriginal != null;
        viewHolderOriginal.textValue.setTag(list.get(position));
        int color = 0;
        int im = 0;
        switch (position) {
            case 0:
                color = 0xFF7653FF;
                im = ResourceTable.Graphic_tree;
                break;
            case 1:
                color = 0xFFFFD325;
                im = ResourceTable.Graphic_tree1;
                break;
            case 3:
                color = 0xFFFF28C8;
                im = ResourceTable.Graphic_tree2;
                break;
            case 2:
                color = 0xFF4DFF18;
                im = ResourceTable.Graphic_tree3;
                break;
            case 4:
                color = 0xFF8AFFF5;
                im = ResourceTable.Graphic_tree4;
                break;
            case 5:
                color = 0xFFFF4C51;
                im = ResourceTable.Graphic_tree5;
                break;
            default:
                color = 0xFF7653FF;
                im = ResourceTable.Graphic_tree6;
        }
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(color));
        viewHolderOriginal.itemView.setBackground(shapeElement);
        try {
            viewHolderOriginal.image.setPixelMap(preparePixelmap(viewHolderOriginal.image.getResourceManager()
                    .getResource(im)));
        } catch (IOException | NotExistException e) {
            LogUtil.error("View", "" + e.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    static class ViewHolderOriginal extends RecyclerContainer.ViewHolder {
        private Text textValue;
        private Image image;

        ViewHolderOriginal(Component itemView) {
            super(itemView);
            if (itemView.findComponentById(ResourceTable.Id_text) instanceof Text) {
                textValue = (Text) itemView.findComponentById(ResourceTable.Id_text);
            }
            if (itemView.findComponentById(ResourceTable.Id_image) instanceof Image) {
                image = (Image) itemView.findComponentById(ResourceTable.Id_image);
            }
        }
    }

    private static PixelMap preparePixelmap(Resource resource) throws IOException, NotExistException {
        ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
        ImageSource imageSource = null;
        try {
            imageSource = ImageSource.create(readResource(resource), srcOpts);
        } finally {
            resource.close();
        }
        if (imageSource == null) {
            LogUtil.debug("File", "Not Found");
        }
        ImageSource.DecodingOptions decodingOpts = new ImageSource.DecodingOptions();
        decodingOpts.desiredSize = new Size(0, 0);
        decodingOpts.desiredRegion = new Rect(0, 0, 0, 0);
        decodingOpts.desiredPixelFormat = PixelFormat.ARGB_8888;

        return imageSource.createPixelmap(decodingOpts);
    }

    private static byte[] readResource(ohos.global.resource.Resource resource) throws IOException {
        final int bufferSize = 1024;
        final int ioEnd = -1;

        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buffer = new byte[bufferSize];
        while (true) {
            try {
                int readLen = resource.read(buffer, 0, bufferSize);
                if (readLen == ioEnd) {
                    break;
                }
                output.write(buffer, 0, readLen);
            } catch (IOException e) {
                break;
            } finally {
                output.close();
            }
        }

        return output.toByteArray();
    }
}